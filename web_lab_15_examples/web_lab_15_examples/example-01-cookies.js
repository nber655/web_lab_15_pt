// Setup code
// --------------------------------------------------------------------------
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// Specify that the app should use cookie-parser to read / write cookies. Omnomnom.
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// We could do this instead, which will allow us to "sign" our cookies. This can help unauthorized modifications.
// app.use(cookieParser("some secret"));

// --------------------------------------------------------------------------

// Route handlers
// --------------------------------------------------------------------------

app.get('/', function (req, res) {

    // All cookies can be obtained from req.cookies.
    // To get a cookie named "hello", you can use req.cookies.hello, or req.cookies["hello"].
    // You can also loop through all cookies using the for..in loop, as shown below.

    var cookies = [];
    if (req.cookies) {
        for (var cookieName in req.cookies) {
            var cookieValue = req.cookies[cookieName];
            cookies.push({ name: cookieName, value: cookieValue });
        }
    }

    // req.signedCookies allows us to get our signed cookies.
    // if (req.signedCookies) {
    //     for (var cookieName in req.signedCookies) {
    //         var cookieValue = req.signedCookies[cookieName];
    //         cookies.push({ name: cookieName, value: cookieValue });
    //     }
    // }

    res.render("example-01-cookies", { cookies: cookies });
});

app.post('/addCookie', function (req, res) {

    // Make a new cookie from the data the user entered
    var cookieName = req.body.name;
    var cookieValue = req.body.value;

    // At minimum, cookies need a name and a value.
    res.cookie(cookieName, cookieValue);

    // We could give the cookie an expiry maximum age too. The cookie will automatically be deleted if it passes
    // this age, in milliseconds. This example will give the cookie an expiry time of 100 seconds after it's created.
    // res.cookie(cookieName, cookieValue, { maxAge: 100000 });

    // We can also sign our cookies if we want - can help to prevent modification.
    // res.cookie(cookieName, cookieValue, { signed: true });

    // Redirect back to homepage
    res.redirect("/");
});

app.post('/removeCookie', function (req, res) {

    if (req.body.toRemove) {

        // Get names of cookies to delete.
        // Remember - req.body.toRemove might be an array, or a single item, depending on the user's selection.
        var cookiesToRemove;
        if (Array.isArray(req.body.toRemove)) {
            cookiesToRemove = req.body.toRemove;
        } else {
            cookiesToRemove = [req.body.toRemove];
        }

        for (var i = 0; i < cookiesToRemove.length; i++) {

            // Delete each of the cookies we want to delete
            res.clearCookie(cookiesToRemove[i]);
        }

    }

    // Redirect back to homepage
    res.redirect("/");
});

// --------------------------------------------------------------------------

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});