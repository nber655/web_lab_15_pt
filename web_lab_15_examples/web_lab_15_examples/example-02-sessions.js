// Setup code
// --------------------------------------------------------------------------
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// Specify that the app should use express-session to create in-memory sessions
var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "compsci719"
}));

// --------------------------------------------------------------------------

// Route handlers
// --------------------------------------------------------------------------

app.get('/', function (req, res) {

    // All session data can be read from req.session.
    // Again, data can be read just like any other JavaScript object.
    // e.g. to get the sessions "name" property, you can use:
    // req.session.name, OR
    // req.session["name"]
    // Or you can loop through all properties in the session using the for..in loop as shown here.

    var data = [];
    if (req.session) {
        console.log(req.session);
        for (var name in req.session) {
            if (name != "cookie") { // Sessions might have a "cookie" value, which we don't want to send back to the client.
                var value = req.session[name];
                data.push({ name: name, value: value });
            }
        }
    }

    res.render("example-02-sessions", { data: data });
});

app.post('/addData', function (req, res) {

    // To add session data:
    // If you already know the name of the data (e.g. you know you want to add a "name" property)
    // you can use code like this:
    // e.g. req.session.name = "Thomas";

    // If the property name is in a variable, such as it is in this case (req.body.name)
    // we can use the alternative syntax:
    req.session[req.body.name] = req.body.value;

    // Note that session data can be anything - it doesn't have to just be Strings. It can be any JSON.

    // Redirect back to homepage
    res.redirect("/");
});

app.post('/removeData', function (req, res) {

    if (req.body.toRemove) {

        // Get names of data properties to delete.
        // Remember - req.body.toRemove might be an array, or a single item, depending on the user's selection.
        var toRemove;
        if (Array.isArray(req.body.toRemove)) {
            toRemove = req.body.toRemove;
        } else {
            toRemove = [req.body.toRemove];
        }

        for (var i = 0; i < toRemove.length; i++) {

            // JavaScript has a standard function called "delete", which we can use
            // to delete object properties. As each data item in the session is simply an object property,
            // we can just use that.
            delete req.session[toRemove[i]];
        }

    }

    // Redirect back to homepage
    res.redirect("/");
});

// --------------------------------------------------------------------------

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});